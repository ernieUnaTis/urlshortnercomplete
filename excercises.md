

#1. Write a function that returns product of all numbers of an array/range

def product(array)
	array.reduce(:*)
end


#2. Compare if two strings are anagrams (assume input consists of ASCII alphabets only)
def is_anagram?(string1,
				string2)
	return false if string1.size != string2.size
	return string1.to_s.chars.sort.join('').downcase==string2.to_s.chars.sort.join('').downcase
end

#3. Compare if two strings are same irrespective of case
def is_same_string?(string1,
				    string2)
	string1 = string1.to_s.downcase
	string2 = string2.to_s.downcase
	return string1.chars.sort.join('')==string2.chars.sort.join('')
end

#Ejemplos
puts product([1,2,3,4])
puts product([1])
puts product([-4, 2.3e12, 77.23, 982, 0b101])
puts product([1,2,3,6,9])

puts is_anagram?("abc",0)
puts is_anagram?("abc","d")
puts is_anagram?("abc","def")
puts is_anagram?("ernesto","otsenre")

puts is_same_string?("ernesto",0)
puts is_same_string?("ernesto","ErNESto")
puts is_same_string?("ernesto","ErNESt0")