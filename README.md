1.- En este repositorio viene los ejercicios y el Docker compose

2.- Bajar en el mismo path los siguientes proyectos:
https://gitlab.com/ernieUnaTis/urlshortener-factory

https://gitlab.com/ernieUnaTis/urlshortener


3.- Ejecutar el docker-compose, el frontend ( ReactJs corre por el puerto 3001) y el backend (Rails) es por el puerto 3000


4.- LA bd que use fue sqlite3

5.- Usuario creado: edut83@gmail.com pass:123456

6.- Las urls con API para probar son:
http://127.0.0.1:3000/obtener_todos_links  header: apikey:12345

curl -X  POST -H 'apikey: 12345'  -H "Content-Type: application/json" http://127.0.0.1:3000/crear_short_url -d '{"url":"http://www.google.com"}'


7.- Para el algoritmo de shortUrl,use el algoritmo de 62 (las letras del abecedario) diviendo el id del registro, y le concateno el paerte del email cifrado con SHA1