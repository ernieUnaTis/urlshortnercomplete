class Link < ApplicationRecord

	validates :url, format: { with: URI::regexp(%w(http https)), message: 'You provided invalid URL' }
	

	after_create :save_short_url
	belongs_to   :user
	has_many     :link_logs

	MAP = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	

	def save_short_url
		self.code = generate_short_url(self.id,self.user_id)
  		self.save
	end

	def generate_short_url(id,user_id)
		email = User.find(user_id).email
		short_url = ""
		while id > 0  do 
			short_url += MAP[id % 62]
			id = id/62
		end
		if(short_url.length < 6)
			short_url = short_url.to_s + Digest::SHA1.hexdigest(email)[8..16].to_s
		end
		return short_url
	end



end
