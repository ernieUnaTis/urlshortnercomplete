class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  after_create :add_name_and_apikey
  has_many :links
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def add_name_and_apikey
  	self.apikey =SecureRandom.base58(24)
  	self.save
  end

end
