class LinkSerializer
  include FastJsonapi::ObjectSerializer
  attributes :url, :user_id, :code, :visits_count,:created_at

  attributes :stats do |object|
    object.link_logs.group(:user_agent).order('count_all desc').count.as_json
  end


end
