class LinkStatsSerializer
  include FastJsonapi::ObjectSerializer
  attributes :user_agent, :count
end
