class LinksController < ApplicationController
	PER_PAGE=3
	before_action      :set_user_by_api_key, only: [:get_my_short_urls,:create_short_url]

	#POST /links
	def create
		@link = Link.create!(links_params)
    	if(@link.save)
			render json: LinkSerializer.new(@link), status: :created
		else
			render json: ErrorSerializer.serialize(@link.errors), status: :bad_request
		end
	end

	#POST /user_links
	def user_links
		@links = User.find(params[:user_id]).links.order('created_at DESC')
    	render json: LinkSerializer.new(@links).serialized_json, status: :ok
	end

	#GET /links
	def index
		@links = Link.all
		render json: LinkSerializer.new(@links).serialized_json, status: :ok
	end

	#GET /links/:id
	def show
		@link = Link.find(params[:id])
		@stats = @link.link_logs.group(:user_agent).order('count_all desc').count
		if @link
			render json: LinkSerializer.new(@link).serialized_json, status: :ok
		else
			render json: ErrorSerializer.new(@link.errors), status: :error
			#render json: "{NOK}", status: :error
		end
	end

	#PUT /links/:id
	def update
		@link = Link.find(params[:id])
		if @link
			@link.update(links_params)
			render json: LinkSerializer.new(@links), status: :ok
		else
			head :not_found,  status: :not_found
		end
	end

	#DELETE /links/:id
	def destroy
		@link = Link.find(params[:id])
		if @link
			@link.destroy
			head :no_content, status: :no_content
		else
			head :not_found,  status: :not_found
		end
	end

	def redirect_to_site
		ip = request.remote_ip
		user_agent = request.user_agent
		link = Link.find_by_code(params[:s])
		link.link_logs.create!(:ip=>ip,:user_agent=>user_agent)
		redirect_to link.url
	end


	#WS with ApiKey
	def get_my_short_urls
		@links = @user.links.order('created_at DESC').page(params[:page] || 1).per(2)
		  options = {
		    links: {
  					links: paginate(@links), 
		    }
		  }
    	render json: LinkSerializer.new(@links), status: :ok
	end
	
	def create_short_url
		@link = @user.links.create!(links_params)
		if(@link.save)
			render json: LinkSerializer.new(@link), status: :created
		else
			render json: ErrorSerializer.serialize(@link.errors), status: :bad_request
		end
	end
	
	private
	def links_params
		params.require(:link).permit(:user_id,:url)
	end



	#Pagination
	def set_pagination_headers
      pages = @links
      headers["X-Total-Count"] = pc.total_count
      links = []
      links << page_link(1, "first") unless pages.first_page?
      links << page_link(pc.prev_page, "prev") if pages.prev_page
      links << page_link(pc.next_page, "next") if pages.next_page
      links << page_link(pc.total_pages, "last") unless pages.last_page?
      headers["Link"] = links.join(", ") if links.present?
    end

    def page_link(page, rel)
      # "<#{posts_url(request.query_parameters.merge(page: page))}>; rel='#{rel}'"
      base_uri = request.url.split("?").first
      "<#{base_uri}?#{request.query_parameters.merge(page: page).to_param}>; rel='#{rel}'"
    end 


	  def current_page
	    (params[:page] || 1).to_i
	  end
	  
	  def per_page
	    (params[:per_page] || 3).to_i
	  end

end
