class SessionsController < Devise::SessionsController

    respond_to :json

    def new
    	self.resource = resource_class.new(sign_in_params)
    	clean_up_passwords(resource)
    	if(resource.id!=nil)
    		render json: UserSerializer.new(resource), status: :ok
    	else
    		render json: ErrorSerializer.serialize(resource.errors), status: :not_found
    	end
  	end

  	def create
    	self.resource = warden.authenticate!(auth_options)
    	sign_in(resource_name, resource)
    	yield resource if block_given?
    	if(resource.id!=nil)
    		render json: UserSerializer.new(resource), status: :ok
    	else
    		render json: ErrorSerializer.serialize(resource.errors), status: :not_found
    	end
  	end



	private
	def respond_with(resource, _opts={})
		render json:resource
	end

	def respond_to_on_destroy
		head :no_content
	end
	
end
