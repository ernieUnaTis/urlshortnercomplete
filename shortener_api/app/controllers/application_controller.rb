class ApplicationController < ActionController::API
	before_action :configure_permitted_parameters, if: :devise_controller?

	include ErrorSerializer

	
	rescue_from ActiveRecord::RecordInvalid do |e|
		render serializer: ErrorSerializer,json: {error: {RecordInvalid: e.record.errors}}, status: 406
	end

	rescue_from ActiveRecord::RecordNotFound do
  		render serializer: ErrorSerializer,json: {error: {RecordNotFound: "Record not found for id: #{params[:id]}"}}, status: 404    
	end 

	def set_user_by_api_key(mapping = nil)
		api_key = request.headers[:apikey]
		if api_key.nil?
			render serializer: ErrorSerializer, json: {:status => 'failed', :errors => 'apikey null'}, status: :unauthorized
		else
			@user = User.find_by_apikey(api_key)
			if(@user.nil?)
				render serializer: ErrorSerializer, json: {:status => 'failed', :errors => 'apikey wrong'}, status: :unauthorized
			end
		end
	end

	

	protected

  	def configure_permitted_parameters
    	devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email])
    end
end
