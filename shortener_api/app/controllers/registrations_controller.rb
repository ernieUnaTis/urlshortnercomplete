class RegistrationsController < Devise::RegistrationsController
	respond_to :json


	def create
		build_resource(sign_up_params)
	 
		if resource.save
    		sign_up(resource_name, resource) if resource.persisted?
			render json: UserSerializer.new(resource), status: :created
		else
			render json: ErrorSerializer.serialize(resource.errors), status: :bad_request
		end
	end

end
