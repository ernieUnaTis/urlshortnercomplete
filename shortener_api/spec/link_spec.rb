require 'rails_helper'

RSpec.describe 'Link', type: :request do

	describe 'Show Link Succesful' do
  		it 'returns success' do
    		get '/links/1'
		  	expect(response).to have_http_status(:ok)
    	end
  	end

  	describe 'Show Link Failed' do
  		it 'returns success' do
    		get '/links/25'
		  	expect(response).to have_http_status(:not_found)
    	end
  	end

  	describe 'Show Link Succesful' do
  		it 'returns success' do
    		post '/user_links',params: {user_id: "3"}
		  	expect(response).to have_http_status(:ok)
    	end
  	end

  	describe 'Show Link Failed' do
  		it 'returns success' do
    		post '/user_links',params: {user_id: "404"}
		  	expect(response).to have_http_status(:not_found)
    	end
  	end

  	describe 'Create Link Succesful' do
  		it 'returns success' do
    		post '/links',params: {"link":{"user_id":3,"url":"https://github.com"}}
		  	expect(response).to have_http_status(:created)
    	end
  	end

  	describe 'Create Link Failed FORMAT URL BAD' do
  		it 'returns success' do
    		post '/links',params: {"link":{"user_id":3,"url":"ithub.com"}}
		  	expect(response).to have_http_status(:not_acceptable)
    	end
  	end

  	describe 'Create Link Failed BAD REQUEST' do
  		it 'returns success' do
    		post '/links',params: {"link":{"user_id":1,"url":"ithub.com"}}
		  	expect(response).to have_http_status(:not_acceptable)
    	end
  	end

end