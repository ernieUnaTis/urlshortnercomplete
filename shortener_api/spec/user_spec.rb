require 'rails_helper'

RSpec.describe 'User', type: :request do
  
  describe 'Login Success' do
  	it 'returns success' do
    	post '/login',params: {email: "edut83@gmail.com", password: "1234567890" }
		  expect(response).to have_http_status(:created)
    end
  end

  describe 'Login failed' do
  	it 'returns success' do
    	post '/login',params: {email: "edut85@gmail.com", password: "123456" }
		  expect(response).to have_http_status(:not_found)
    end
  end

  describe 'Create User success' do
  	it 'returns success' do
    	post '/signup',params: {email: "danny13@gmail.com", password: "123456",name:"Daniela De Unanue"}
		  expect(response).to have_http_status(:created)
    end
  end

   describe 'Create User  email existens' do
  	it 'returns success' do
    	post '/signup',params: {email: "edut83@gmail.com", password: "123456",name:"Daniela"}
		  expect(response).to have_http_status(:bad_request)
    end
  end

  describe 'Create User Bad Request'  do
  	it 'returns success' do
    	post '/signup',params: {email: "", password: "123456",name:"" }
		  expect(response).to have_http_status(:bad_request)
    end
  end

end