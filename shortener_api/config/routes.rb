Rails.application.routes.draw do
  devise_for :users, skip: %i[registrations sessions passwords]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  devise_scope :user do
  	post   '/signup', to: 'registrations#create'
  	post   '/login',  to: 'sessions#create'
  	delete '/logout', to: 'sessions#destroy'
  end
  resources :users
  resources :links

  get ':s',to: "links#redirect_to_site" 
  post 'user_links',to: "links#user_links" 
  post 'get_my_short_urls',to: "links#get_my_short_urls" 
  post 'create_short_url',to: "links#create_short_url"


end
